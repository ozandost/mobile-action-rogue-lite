public enum GameState
{
    Loading,
    MainMenu,
    InGame,
    Paused,
    Fail,
    Victory
}