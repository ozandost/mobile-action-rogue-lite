
using DungeonSystem;
using Enemies;

public class GameStateChangedSignal : ASignal<GameState, GameState> { }



//Game Signals

public class DungeonCreated : ASignal<Dungeon> { }


//Combat Signals

public class OnEnemyHit : ASignal<Enemy> { }
public class OnEnemyDied : ASignal<Enemy> { }

