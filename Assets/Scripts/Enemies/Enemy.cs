using System;
using UnityEngine;
using Weapons;

namespace Enemies
{
    public class Enemy : MonoBehaviour
    {
        public float totalHealth;
        public float CurrentHealth { get; private set; }
        public EnemyState State { get; private set; }

        public event Action<Enemy, EnemyState /*old state*/, EnemyState /*new State*/> StateChanged;

        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            CurrentHealth = totalHealth;
        }

        public void TakeDamage(float damage)
        {
            CurrentHealth -= damage;


            if (CurrentHealth <= 0f)
            {
                SetState(EnemyState.Dead);
                Destroy(this.gameObject);
            }
        }

        public void GetHit(BWeapon weapon)
        {
            TakeDamage(weapon.baseDamage);
            Debug.Log($"Enemy health {CurrentHealth}");
        }


        public void SetState(EnemyState newState)
        {
            StateChanged?.Invoke(this, this.State, newState);
            this.State = newState;
        }
    }
}