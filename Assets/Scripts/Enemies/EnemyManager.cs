using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Enemies
{
    public class EnemyManager : MonoBehaviour
    {
        public List<Enemy> Enemies { get; private set; }
        public static event Action<Enemy> EnemyDied;

        //testing
        private int _lastBatchCount;
        private int _deadCount;

        private void Awake()
        {
            Initialize();
        }

        public void Initialize()
        {
            // Enemies = transform.parent.GetComponentsInChildren<Enemy>(true).ToList();
            // Enemies.ForEach(enemy => enemy.StateChanged += Enemy_OnStateChanged);
        }

        private void Enemy_OnStateChanged(Enemy enemy,EnemyState oldState, EnemyState newState)
        {
            if(newState == EnemyState.Dead) EnemyDied?.Invoke(enemy);
        }
    }
}