namespace Enemies
{
    public enum EnemyState
    {
        Idle,
        Targetting,
        Attacking,
        Dead,
    }
}
