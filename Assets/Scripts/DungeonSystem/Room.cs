using UnityEngine;

namespace DungeonSystem
{
    public class Room : MonoBehaviour
    {
        [Header("References")] public BoxCollider boundCollider;
        public Gate gate;
        public Transform entryPoint;

        public void Initialize()
        {
            gameObject.SetActive(true);
        }

        public void Finalize()
        {
            gameObject.SetActive(false);
        }
    }
}