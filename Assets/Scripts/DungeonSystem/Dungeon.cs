using System.Collections.Generic;
using UnityEngine;

namespace DungeonSystem
{
    public class Dungeon : MonoBehaviour
    {
        [Header("References")] public Room room;
        public List<Room> Rooms { get; private set; }
        public int PlayerRoomIndex { get; private set; }
        public Room CurrentRoom => Rooms[PlayerRoomIndex];
        public DungeonData Data { get; private set; }
        
        public void Initialize(DungeonData data)
        {
            Data = data;

            CreateRooms();
            PlayerRoomIndex = 0;

            ActivateCurrentRoom();
        }

        private void CreateRooms()
        {
            Rooms = new List<Room>();
            for (int i = 0; i < Data.totalRoomCount; i++)
            {
                var roomObject = Instantiate(room, transform);
                Rooms.Add(roomObject);
            }
        }

        private void ActivateCurrentRoom()
        {
            foreach (var r in Rooms)
            {
                r.gameObject.SetActive(false);
            }

            Rooms[PlayerRoomIndex].Initialize();
        }

        public void Finalize()
        {
        }
    }
}