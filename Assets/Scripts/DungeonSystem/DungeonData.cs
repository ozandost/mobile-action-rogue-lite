using UnityEditor;
using UnityEngine;

namespace DungeonSystem
{
    [CreateAssetMenu(fileName = "DungeonData", menuName = "Dost/Create Dungeon Data", order = 1)]
    public class DungeonData : ScriptableObject
    {
        public int totalRoomCount;
        public int healRoomCount;
        public int chestRoomCount;
        public int bossRoomCount;
    }
}