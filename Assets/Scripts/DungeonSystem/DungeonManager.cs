using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace DungeonSystem
{
    public class DungeonManager : MonoBehaviour
    {
        [Header("References")] public Dungeon dungeonPrefab;
        public DungeonData[] dungeonDatas;
        public Dungeon CurrentDungeon { get; private set; }

        private Dungeon _lastDungeon;


        public void CreateDungeon()
        {
            if (_lastDungeon != null) _lastDungeon.Finalize();
            CurrentDungeon = Instantiate(dungeonPrefab, transform);
            CurrentDungeon.Initialize(dungeonDatas[Random.Range(0, dungeonDatas.Length)]);

            _lastDungeon = CurrentDungeon;

            Signals.Get<DungeonCreated>().Dispatch(CurrentDungeon);

        }
    }
}