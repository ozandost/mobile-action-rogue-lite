﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Utilities.Input
{
    public class InputHandler : MonoBehaviour
    {
        public DistanceType distanceType = DistanceType.Inches;
        public bool useCustomDPI = true;
        public float customDPI = 326f;
        public float swipeThresholdInches = 3f;
        public float swipeThresholdPixels = 800f;
        public bool isActiveJoystickVisual = true;
        public bool isFloatingJoystickCenterPoint = true;
        public float joystickDragThresholdInches = 0.05f;
        public float joystickMaxDistanceInches = 0.5f;
        public float joystickDragThresholdPixels = 10f;
        public float joystickMaxDistancePixels = 250f;

        public static float ScreenDPI { get; private set; }
        public static Vector2 DeltaMousePosition { get; private set; }
        public static Vector2 DeltaMouseInchesPosition => DeltaMousePosition / ScreenDPI;
        public static Vector2 DragVector => _lastMousePosition - _firstMousePosition;
        public static float DragMovementTime => _dragTime - _mouseDownTime;
        public static Vector2 DragVelocityInches => (DragVector / ScreenDPI) / DragMovementTime;
        public static Vector2 DragVelocityPixels => DragVector / DragMovementTime;
        public static SwipeDirection SwipeDirection { get; private set; }
        public static Camera GameCamera { get; private set; }
        public static bool IsActiveJoystick { get; private set; }
        public static Vector2 JoystickCenter { get; private set; } = Vector2.zero;
        public static Vector2 JoystickPosition { get; private set; } = Vector2.zero;

        private static InputHandler _instance;
        
        private static Vector3 _lastMousePosition;
        private static Vector3 _firstMousePosition;
        private static Vector3 _lastRayWorldPosition;
        private static Vector3 _firstRayWorldPosition;
        private static bool _isActiveDeltaScreenPositionWithRay;
        private static float _mouseDownTime;
        private static float _dragTime;
        private float _currentMaxDistance;

        public enum DistanceType
        {
            Inches,
            Pixels
        }

        private void Awake()
        {
            if (_instance == null)
            {
                _instance = this;
            }
            else
            {
                Destroy(this);
                return;
            }

            ScreenDPI = GetScreenDPI();
            
            SetGameCamera(Camera.main);
            DisableJoystick();
        }

        private void Update()
        {
            if (GetMouseButtonDown())
            {
                _firstMousePosition = GetMousePosition();
                _lastMousePosition = _firstMousePosition;
                DeltaMousePosition = Vector2.zero;
                _mouseDownTime = Time.time;
                UpdateJoystick();
            }
            else if (GetMouseButton() || GetMouseButtonUp())
            {
                var mousePosition = GetMousePosition();
                DeltaMousePosition = mousePosition - _lastMousePosition;
                _lastMousePosition = mousePosition;
                _dragTime = Time.time;

                if (GetMouseButtonUp())
                {
                    _isActiveDeltaScreenPositionWithRay = false;
                    DisableJoystick();
                }
                else
                {
                    UpdateJoystick();
                }
            }
            else
            {
                DeltaMousePosition = Vector2.zero;
                DisableJoystick();
            }

            GetSwipeDirection();
        }

        private void UpdateJoystick()
        {
            CalculateJoystick();
        }
        
        private void CalculateJoystick()
        {
            if (!IsActiveJoystick)
            {
                JoystickCenter = GetMousePosition();
                IsActiveJoystick = true;
                _currentMaxDistance = distanceType == DistanceType.Pixels ? 
                    joystickMaxDistancePixels : joystickMaxDistanceInches * ScreenDPI;
            }

            JoystickPosition = GetMousePosition();

            var joystickDistance = JoystickPosition - JoystickCenter;
            if (isFloatingJoystickCenterPoint)
            {
                if (joystickDistance.magnitude > _currentMaxDistance)
                {
                    var direction = joystickDistance.normalized;
                    JoystickCenter = JoystickPosition - direction * _currentMaxDistance;
                }
            }

            joystickDistance = Vector2.ClampMagnitude(joystickDistance, _currentMaxDistance);
            JoystickPosition = JoystickCenter + joystickDistance;
        }

        private void DisableJoystick()
        {
            IsActiveJoystick = false;
        }
        
        private void GetSwipeDirection()
        {
            SwipeDirection = SwipeDirection.None;
            if (!GetMouseButtonUp()) return;

            var velocity = distanceType == DistanceType.Pixels ? DragVelocityPixels : DragVelocityInches;
            var threshold = distanceType == DistanceType.Pixels ? swipeThresholdPixels : swipeThresholdInches;
            
            if (velocity.magnitude > threshold)
            {
                if (Mathf.Abs(velocity.x) > Mathf.Abs(velocity.y))
                {
                    SwipeDirection = velocity.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                }
                else
                {
                    SwipeDirection = velocity.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;
                }
            }
        }

        public static float GetHorizontal()
        {
            var total = UnityEngine.Input.GetAxisRaw("Horizontal");
            total += GetJoystickInput().x;
            return Mathf.Clamp(total,-1,1);
        }

        public static float GetVertical()
        {
            var total = UnityEngine.Input.GetAxisRaw("Vertical");
            total += GetJoystickInput().y;
            return Mathf.Clamp(total, -1, 1);
        }

        public static Vector2 GetInput()
        {
            return new Vector2(GetHorizontal(), GetVertical());
        }

        public static Vector3 Get3DInput()
        {
            return new Vector3(GetHorizontal(), 0, GetVertical());
        }

        public static Vector2 GetJoystickInput()
        {
            if (!IsActiveJoystick)
            {
                return Vector2.zero;
            }
            
            var joystickDistance = JoystickPosition - JoystickCenter;

            var isOverThreshold = false;
            if (_instance.distanceType == DistanceType.Pixels)
            {
                isOverThreshold = joystickDistance.magnitude > _instance.joystickDragThresholdPixels;
            }
            else
            {
                isOverThreshold = joystickDistance.magnitude > _instance.joystickDragThresholdInches * ScreenDPI;
            }
            
            return isOverThreshold ? 
                Vector2.ClampMagnitude(joystickDistance / _instance._currentMaxDistance, 1f) : Vector2.zero;
        }

        public static bool GetMouseButtonDown()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            return Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began;
#else
            return UnityEngine.Input.GetMouseButtonDown(0);
#endif
        }

        public static bool GetMouseButton()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            if (Input.touchCount <= 0) return false;
            
            var touchPhase = Input.GetTouch(0).phase;
            return touchPhase != TouchPhase.Ended && touchPhase != TouchPhase.Canceled;
#else
            return UnityEngine.Input.GetMouseButton(0);
#endif
        }

        public static bool GetMouseButtonUp()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            if (Input.touchCount <= 0) return false;

            var touchPhase = Input.GetTouch(0).phase;
            return touchPhase == TouchPhase.Ended || touchPhase == TouchPhase.Canceled;
#else
            return UnityEngine.Input.GetMouseButtonUp(0);
#endif
        }
        
        public static Vector3 GetMousePosition()
        {
            var mousePosition = Vector3.zero;

            if (GetMouseButton() || GetMouseButtonDown() || GetMouseButtonUp())
            {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
                mousePosition = Input.GetTouch(0).position;
#else
                mousePosition = UnityEngine.Input.mousePosition;
#endif
            }

            return mousePosition;
        }
        
        public static bool RaycastWithMousePosition(float distance, LayerMask layerMask, Vector3? offset = null)
        {
            if (GetMouseButtonDown() || GetMouseButton() || GetMouseButtonUp())
            {
                var ray = RayForRaycastWithMousePosition(offset ?? Vector3.zero);
                return Physics.Raycast(ray, distance, layerMask);
            }
            
            return false;
        }
        
        [Obsolete("Use \"RaycastWithMousePosition(out RaycastHit raycastHit, float distance, Vector3? offset = null)\".")]
        public static bool RaycastWithMousePosition(Vector3 offset, out RaycastHit raycastHit, float distance)
        {
            if (GetMouseButtonDown() || GetMouseButton() || GetMouseButtonUp())
            {
                var ray = RayForRaycastWithMousePosition(offset);
                return Physics.Raycast(ray, out raycastHit, distance);
            }
            
            raycastHit = new RaycastHit();
            return false;
        }

        public static bool RaycastWithMousePosition(out RaycastHit raycastHit, float distance, Vector3? offset = null)
        {
            if (GetMouseButtonDown() || GetMouseButton() || GetMouseButtonUp())
            {
                var ray = RayForRaycastWithMousePosition(offset ?? Vector3.zero);
                return Physics.Raycast(ray, out raycastHit, distance);
            }
            
            raycastHit = new RaycastHit();
            return false;
        }
        
        [Obsolete("Use \"RaycastWithMousePosition(out RaycastHit raycastHit, float distance, LayerMask layerMask, Vector3? offset = null)\".")]
        public static bool RaycastWithMousePosition(Vector3 offset, out RaycastHit raycastHit, float distance, LayerMask layerMask, QueryTriggerInteraction triggerInteraction = QueryTriggerInteraction.UseGlobal)
        {
            if (GetMouseButtonDown() || GetMouseButton() || GetMouseButtonUp())
            {
                var ray = RayForRaycastWithMousePosition(offset);
                return Physics.Raycast(ray, out raycastHit, distance, layerMask, triggerInteraction);
            }
            
            raycastHit = new RaycastHit();
            return false;
        }
        
        public static bool RaycastWithMousePosition(out RaycastHit raycastHit, float distance, LayerMask layerMask, Vector3? offset = null)
        {
            if (GetMouseButtonDown() || GetMouseButton() || GetMouseButtonUp())
            {
                var ray = RayForRaycastWithMousePosition(offset ?? Vector3.zero);
                return Physics.Raycast(ray, out raycastHit, distance, layerMask);
            }
            
            raycastHit = new RaycastHit();
            return false;
        }

        public static bool RaycastWithMousePosition(out RaycastHit raycastHit, float distance, Collider collider, Vector3? offset = null)
        {
            if (GetMouseButtonDown() || GetMouseButton() || GetMouseButtonUp())
            {
                var ray = RayForRaycastWithMousePosition(offset ?? Vector3.zero);
                return collider.Raycast(ray, out raycastHit, distance);
            }
            
            raycastHit = new RaycastHit();
            return false;
        }

        public static Ray RayForRaycastWithMousePosition(Vector3 offset)
        {
            return GameCamera.ScreenPointToRay(GetMousePosition() + offset * ScreenDPI);
        }

        public static bool DeltaWorldPositionWithRay(out Vector3 deltaPosition, out RaycastHit raycastHit, float distance, LayerMask layerMask)
        {
            if (RaycastWithMousePosition(out raycastHit, distance, layerMask))
            {
                if (GetMouseButtonDown())
                {
                    _firstRayWorldPosition = raycastHit.point;
                    _lastRayWorldPosition = _firstRayWorldPosition;
                }
                else if (GetMouseButton())
                {
                    _lastRayWorldPosition = raycastHit.point;
                }
            }
            else
            {
                if (!GetMouseButton())
                {
                    deltaPosition = Vector3.zero;
                    return false;
                }
            }

            deltaPosition = _lastRayWorldPosition - _firstRayWorldPosition;
            return true;
        }
        
        public static bool DeltaWorldPositionWithRay(out Vector3 deltaPosition, out RaycastHit raycastHit, float distance, Collider collider)
        {
            if (RaycastWithMousePosition(out raycastHit, distance, collider))
            {
                if (GetMouseButtonDown())
                {
                    _firstRayWorldPosition = raycastHit.point;
                    _lastRayWorldPosition = _firstRayWorldPosition;
                }
                else if (GetMouseButton())
                {
                    _lastRayWorldPosition = raycastHit.point;
                }
            }
            else
            {
                if (!GetMouseButton())
                {
                    deltaPosition = Vector3.zero;
                    return false;
                }
            }

            deltaPosition = _lastRayWorldPosition - _firstRayWorldPosition;
            return true;
        }
        
        public static bool DeltaWorldPositionWithRayForFrame(out Vector3 deltaPosition, out RaycastHit raycastHit, float distance, LayerMask layerMask)
        {
            if (!RaycastWithMousePosition(out raycastHit, distance, layerMask))
            {
                deltaPosition = Vector3.zero;
                return false;
            }
            
            if (GetMouseButtonDown())
            {
                _firstRayWorldPosition = raycastHit.point;
                _lastRayWorldPosition = _firstRayWorldPosition;
                deltaPosition = Vector3.zero;
                return false;
            }

            deltaPosition = raycastHit.point - _lastRayWorldPosition;
            _lastRayWorldPosition = raycastHit.point;
            return true;
        }
        
        public static bool DeltaWorldPositionWithRayForFrame(out Vector3 deltaPosition, out RaycastHit raycastHit, float distance, Collider collider)
        {
            if (!RaycastWithMousePosition(out raycastHit, distance, collider))
            {
                deltaPosition = Vector3.zero;
                return false;
            }
            
            if (GetMouseButtonDown())
            {
                _firstRayWorldPosition = raycastHit.point;
                _lastRayWorldPosition = _firstRayWorldPosition;
                deltaPosition = Vector3.zero;
                return false;
            }

            deltaPosition = raycastHit.point - _lastRayWorldPosition;
            _lastRayWorldPosition = raycastHit.point;
            return true;
        }

        public static bool DeltaScreenPositionWithRay(out Vector3 deltaPosition, out RaycastHit raycastHit, float distance, LayerMask layerMask)
        {
            if (RaycastWithMousePosition(out raycastHit, distance, layerMask) && GetMouseButtonDown())
            {
                _isActiveDeltaScreenPositionWithRay = true;
            }

            if (!_isActiveDeltaScreenPositionWithRay)
            {
                deltaPosition = Vector3.zero;
                return false;
            }
            
            if (GetMouseButtonDown())
            {
                _firstMousePosition = GetMousePosition();
                _lastMousePosition = _firstMousePosition;
                deltaPosition = Vector3.zero;
                return false;
            }

            var mousePosition = GetMousePosition();
            deltaPosition = mousePosition - _lastMousePosition;
            _lastMousePosition = mousePosition;
            return true;
        }
        
        public static bool DeltaScreenPositionWithRay(out Vector3 deltaPosition, out RaycastHit raycastHit, float distance, Collider collider)
        {
            if (RaycastWithMousePosition(out raycastHit, distance, collider) && GetMouseButtonDown())
            {
                _isActiveDeltaScreenPositionWithRay = true;
            }

            if (!_isActiveDeltaScreenPositionWithRay)
            {
                deltaPosition = Vector3.zero;
                return false;
            }
            
            if (GetMouseButtonDown())
            {
                _firstMousePosition = GetMousePosition();
                _lastMousePosition = _firstMousePosition;
                deltaPosition = Vector3.zero;
                return false;
            }

            var mousePosition = GetMousePosition();
            deltaPosition = mousePosition - _lastMousePosition;
            _lastMousePosition = mousePosition;
            return true;
        }
        
        public static bool IsPointerOverGameObject()
        {
#if (UNITY_ANDROID || UNITY_IOS) && !UNITY_EDITOR
            return GetMouseButton() && !EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
#else
            return GetMouseButton() && !EventSystem.current.IsPointerOverGameObject();
#endif
        }

        public static void SetGameCamera(Camera gameCamera)
        {
            GameCamera = gameCamera;
        }

        public float GetScreenDPI()
        {
            var dpi = Screen.dpi;
#if UNITY_EDITOR
            if (useCustomDPI)
            {
                dpi = customDPI;
            }
#endif
            return dpi;
        }

        private void OnDrawGizmos()
        {
            if (!isActiveJoystickVisual || !IsActiveJoystick || !Application.isPlaying)
                return;

            DrawCircleScreenSpace(JoystickCenter, Vector3.forward, _currentMaxDistance, 32);
            DrawCircleScreenSpace(JoystickPosition, Vector3.forward, _currentMaxDistance * 0.2f, 32);
        }

        public static void DrawCircleScreenSpace(Vector3 center, Vector3 normal, float radius, int segmentCount = 16)
        {
            Vector3 upVector = Mathf.Abs(Vector3.Dot(normal, Vector3.up)) >= 0.95f ? Vector3.right : Vector3.up;

            Vector3 axis1 = Vector3.Cross(normal, upVector).normalized;
            Vector3 axis2 = Vector3.Cross(normal, axis1).normalized;

            float currentAngle = 0f;
            Vector3 prePoint = center + radius * (axis1 * Mathf.Sin(currentAngle) + axis2 * Mathf.Cos(currentAngle));
            prePoint.z = GameCamera.nearClipPlane + 0.1f;
            prePoint = GameCamera.ScreenToWorldPoint(prePoint);

            for (int i = 1; i <= segmentCount; i++)
            {
                currentAngle = (i / (float)segmentCount) * Mathf.PI * 2;
                Vector3 curPoint = center + radius * (axis1 * Mathf.Sin(currentAngle) + axis2 * Mathf.Cos(currentAngle));
                curPoint.z = GameCamera.nearClipPlane + 0.1f;
                curPoint = GameCamera.ScreenToWorldPoint(curPoint);

                Gizmos.DrawLine(prePoint, curPoint);

                prePoint = curPoint;
            }
        }
    }

    public enum SwipeDirection
    {
        None,
        Left,
        Right,
        Up,
        Down
    }
}