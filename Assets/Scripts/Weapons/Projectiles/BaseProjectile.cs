using System;
using Enemies;
using UnityEngine;
using Weapons;
using Weapons.Test;

public class BaseProjectile : MonoBehaviour
{
    public float Speed { get; private set; }
    public Vector3 TargetDir { get; private set; }
    private bool IsActive { get; set; }
    private float Damage { get; set; }
    public BaseRangeWeapon Weapon { get; private set; }

    public void Initialize(BWeapon weapon, Vector3 dir)
    {
        this.Weapon = (BaseRangeWeapon)weapon;
        this.TargetDir = dir;
        this.Damage = Weapon.CurrentDamage;
        this.Speed = Weapon.projectileSpeed;
        IsActive = true;
    }

    protected virtual void FixedUpdate()
    {
        Move();
    }

    protected virtual void Move()
    {
        if (!IsActive) return;
        transform.position += TargetDir.normalized * (Time.deltaTime * Speed);
    }

    protected void OnTriggerEnter(Collider other)
    {
        IsActive = false;
        if (other.CompareTag("Enemy"))
        {
            var enemy = other.GetComponent<Enemy>();
            enemy.GetHit(Weapon);
            Signals.Get<OnEnemyHit>().Dispatch(enemy);
        }
    }
}