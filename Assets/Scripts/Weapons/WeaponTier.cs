namespace Weapons
{
    public enum WeaponTier
    {
        Common,
        Rare,
        Epic,
        Heroic
    }
}