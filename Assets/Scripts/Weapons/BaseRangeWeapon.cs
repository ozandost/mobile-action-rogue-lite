using UnityEngine;

namespace Weapons
{
    public class BaseRangeWeapon : BWeapon
    {
        public float projectileSpeed;

        protected virtual void Projectile_OnHit(GameObject obj)
        {
            if (obj.CompareTag(EnemyTag))
            {

            }
        }
    }
}