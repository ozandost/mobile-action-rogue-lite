using System;
using System.Collections;
using Enemies;
using UnityEditor.Animations;
using UnityEngine;

namespace Weapons
{
    public class BWeapon : MonoBehaviour
    {
        [Header("Settings")] public int level;
        public float baseDamage;
        public float attackSpeed;
        public WeaponType type;
        public WeaponTier tier;
        public const string EnemyTag = "Enemy";

        [Header("References")] public AnimatorController animationController;

        public bool IsAttacking { get; private set; }
        public float DPS => CurrentDamage * attackSpeed;
        public float CurrentDamage { get; private set; }
        public float CurrentAttackSpeed { get; private set; }
        protected Enemy CurrentTarget { get; private set; }
        protected bool IsActive { get; set; }
        private float AttackInterval => 1f / CurrentAttackSpeed;


        protected IEnumerator basicAttackCoolDown;
        public event Action OnFired;

        private bool _canAttack;
        private float _fireTimer;


        public void Initialize()
        {
            CurrentDamage = baseDamage;
            SetAttackSpeed(attackSpeed);
            IsActive = true;
        }

        protected void SetDamage(float damage)
        {
            CurrentDamage = baseDamage;
        }

        protected void IncreaseDamage(float amount)
        {
            CurrentDamage += amount;
        }

        protected void IncreaseDamageWithPercent(int percent)
        {
            CurrentDamage += CurrentDamage * percent / 100f;
        }

        protected void SetAttackSpeed(float speed)
        {
            CurrentAttackSpeed = speed;
        }

        protected void IncreaseAttackSpeed(float amount)
        {
            CurrentAttackSpeed += amount;
        }

        protected void IncreaseAttackSpeedByPercent(int percent)
        {
            CurrentAttackSpeed += CurrentAttackSpeed * percent / 100f;
        }

        protected virtual void Fire()
        {
            OnFired?.Invoke();
        }

        private void CurrentTargetOnStateChanged(Enemy arg1, EnemyState arg2, EnemyState arg3)
        {
            if (arg3 != EnemyState.Dead)
            {
                arg1.StateChanged -= CurrentTargetOnStateChanged;
            }
        }

        public void ToggleAttack(bool canAttack)
        {
            _canAttack = canAttack;
        }

        protected void Update()
        {
            if (!IsActive) return;
            if (!_canAttack) return;

            _fireTimer += Time.deltaTime;

            if (!(_fireTimer >= AttackInterval)) return;

            _fireTimer = 0f;
            if (CurrentTarget == null) return;
            Fire();
        }

        public void Finalize()
        {
        }

        public void ChangeTarget(Enemy newTarget)
        {
            CurrentTarget = newTarget;
            if (CurrentTarget is null) return;
            CurrentTarget.StateChanged += CurrentTargetOnStateChanged;
        }
    }
}