using Enemies;
using UnityEngine;

namespace Weapons.Test
{
    public class TestStaff : BaseRangeWeapon
    {
        public BaseProjectile fireball;

        protected override void Fire()
        {
            base.Fire();
            var dir = CurrentTarget.transform.position - transform.position;

            var fireball = Instantiate(this.fireball, transform.position, Quaternion.identity);
            fireball.Initialize(this, dir);
        }
    }
}