using System;
using Unity.VisualScripting;
using UnityEngine;

namespace Weapons.Test
{
    public class TestFireball : MonoBehaviour
    {
        public event Action<GameObject> OnHitSomething;
        public event Action OnDestroyed;

        public float Speed { get; private set; }
        public Vector3 TargetDir { get; private set; }
        private bool IsActive { get; set; }

        public void Initialize(Vector3 dir, float speed)
        {
            dir.y = 0;
            TargetDir = dir;
            Speed = speed;
            IsActive = true;
        }

        private void FixedUpdate()
        {
            if (!IsActive) return;
            transform.position += TargetDir.normalized * (Time.deltaTime * Speed);
        }

        private void OnTriggerEnter(Collider other)
        {
            IsActive = false;
            OnHitSomething?.Invoke(other.gameObject);
        }
    }
}