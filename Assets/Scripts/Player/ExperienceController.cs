using Enemies;
using UnityEngine;

namespace Player
{
    public class ExperienceController : MonoBehaviour
    {
        public void Initialize()
        {
            EnemyManager.EnemyDied += EnemyManager_OnEnemyDied;
        }

        private void EnemyManager_OnEnemyDied(Enemy enemy)
        {
            
        }
    }
}