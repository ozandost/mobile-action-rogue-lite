using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Vector3 extraOffset;
    public float followSpeed;

    private Vector3 _initialOffset;
    private Transform _target;

    private Tween _shakeTween;
    public bool IsFollowing { get; private set; }

    public void Initialize(Transform target)
    {
        this._target = target;
    }

    public void Activate()
    {
        if (_target == null)
        {
            Debug.LogError("Target is not assigned");
            return;
        }

        _initialOffset = transform.position - _target.position;
        IsFollowing = true;
    }

    private void Update()
    {
        if (!IsFollowing) return;
        transform.position = Vector3.Lerp(transform.position, _target.position + _initialOffset + extraOffset, followSpeed * Time.deltaTime);
    }

    [Button]
    public void Shake(float duration = 0.15f, Vector3 strength = default, int vibrato = 20)
    {
        _shakeTween?.Kill();
        transform.DOShakePosition(duration, strength, vibrato);
    }
}