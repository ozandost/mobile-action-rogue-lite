using System;
using UnityEngine;
using Utilities.Input;

namespace Player
{
    public class MovementController : MonoBehaviour
    {
        [SerializeField] private CharacterController characterController;

        [Header("Settings")]
        [SerializeField] private float movementSpeed;
        [SerializeField] private float rotationSpeed;
        private BoxCollider BoundCollider { get; set; }
        private float CurrentMovementSpeed { get; set; }
        private bool IsActive { get; set; }
        public static event Action<bool> OnMovementStateChanged;


        private Quaternion _startRotation;

        public void Initialize(BoxCollider boundCollider)
        {
            BoundCollider = boundCollider;
            CurrentMovementSpeed = movementSpeed;
        }

        public void Activate()
        {
            IsActive = true;
        }

        private Vector3 GetInput()
        {
            var input = InputHandler.GetJoystickInput();
            if (input.magnitude > 0) input = input.normalized;
            return new Vector3(input.x, 0, input.y);
        }

        private Vector3 GetClampedPosition(Vector3 pos)
        {
            var bounds = BoundCollider.bounds;
            var x = Mathf.Clamp(pos.x, bounds.min.x, bounds.max.x);
            var z = Mathf.Clamp(pos.z, bounds.min.z, bounds.max.z);

            return new Vector3(x, pos.y, z);
        }

        private void Update()
        {
            if (!IsActive) return;
            if (InputHandler.GetMouseButtonDown())
            {
                OnMovementStateChanged?.Invoke(true);
                _startRotation = transform.rotation;
            }

            if (InputHandler.GetMouseButton())
            {
                var input = GetInput();

                RotatePlayer(input, _startRotation, Time.deltaTime * rotationSpeed);

                characterController.Move(input * CurrentMovementSpeed * Time.deltaTime);
            }
            else
            {
                OnMovementStateChanged?.Invoke(false);
            }
        }

        private void RotatePlayer(Vector3 input, Quaternion startRotation, float t)
        {
            var targetRotation = Quaternion.LookRotation(input, Vector3.up);
            transform.rotation = Quaternion.Slerp(startRotation, targetRotation, t);
        }

        public void ChangeMovementSpeed(float speed)
        {
            CurrentMovementSpeed = speed;
        }

        public void ChangeMovementSpeed(int percent)
        {
            CurrentMovementSpeed -= CurrentMovementSpeed * percent / 100f;
        }

        public void Finalize()
        {
            IsActive = false;
        }
    }
}