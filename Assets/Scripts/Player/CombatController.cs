using System;
using System.Collections;
using Enemies;
using UnityEngine;

namespace Player
{
    public class CombatController : MonoBehaviour
    {
        [Header("Settings")] public LayerMask enemyLayer;
        public float enemyCheckInterval;
        public float radius;
        public float CurrentRadius { get; private set; }
        public Enemy CurrentEnemy { get; private set; }
        public Enemy LastEnemy { get; private set; }

        public static event Action<Enemy> OnTargetChanged;

        private Collider[] _enemyColliders;
        private int _enemyCountInRadius;
        private IEnumerator _enemyCheckRoutine;

        public void Initialize()
        {
            _enemyColliders = new Collider[15];
            _enemyCheckRoutine = EnemyCheckRoutine();
            CurrentRadius = radius;

            MovementController.OnMovementStateChanged += OnMovementStateChanged;
        }

        public void OnMovementStateChanged(bool isMoving)
        {
            if (!isMoving)
            {
                StartCoroutine(_enemyCheckRoutine);
            }
            else
            {
                StopCoroutine(_enemyCheckRoutine);
            }
        }
        

        private void GetEnemyCount()
        {
            _enemyCountInRadius =
                Physics.OverlapSphereNonAlloc(transform.position, CurrentRadius, _enemyColliders, enemyLayer);
        }

        private IEnumerator EnemyCheckRoutine()
        {
            while (true)
            {
                GetEnemyCount();
                FindClosestEnemy();
                yield return new WaitForSeconds(enemyCheckInterval);
            }
        }

        private void FindClosestEnemy()
        {
            if (_enemyCountInRadius == 0)
            {
                if (LastEnemy != null) OnTargetChanged?.Invoke(null);
                return;
            }

            var compareDistance = Mathf.Infinity;
            var closest = _enemyColliders[0];
            foreach (var enemyCollider in _enemyColliders)
            {
                if (enemyCollider != null)
                {
                    var distance = Vector3.Distance(transform.position, enemyCollider.transform.position);
                    if (distance <= compareDistance)
                    {
                        compareDistance = distance;
                        closest = enemyCollider;
                    }
                }
            }

            //todo finalize last enemy
            CurrentEnemy = closest.GetComponent<Enemy>();
            if (LastEnemy != CurrentEnemy)
            {
                OnTargetChanged?.Invoke(CurrentEnemy);
                LastEnemy = CurrentEnemy;
            }
        }


        private void OnDrawGizmos()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(transform.position, radius);
        }
    }
}