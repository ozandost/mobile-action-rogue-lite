using System;
using DungeonSystem;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Player
{
    public class PlayerController : MonoBehaviour
    {
        public MovementController movementController;
        public WeaponController weaponController;
        public CombatController combatController;
        public CameraController cameraController;
        public AnimationController animationController;

        private Dungeon CurrentDungeon { get; set; }
        private Room CurrentRoom => CurrentDungeon.CurrentRoom;

        public static event Action OnEnteredGate;

        private void Awake()
        {
            Signals.Get<DungeonCreated>().AddListener(OnDungeonCreated);
        }

        [Button]
        public void Initialize()
        {

            var targetStartPosition =
                new Vector3(CurrentRoom.entryPoint.position.x, transform.position.y, CurrentRoom.entryPoint.position.z);

            transform.position = targetStartPosition;

            movementController.Initialize(CurrentRoom.boundCollider);
            combatController.Initialize();
            weaponController.Initialize();
            cameraController.Initialize(transform);

            Activate();
        }

        [Button]
        public void Activate()
        {
            movementController.Activate();
            cameraController.Activate();
        }

        public void OnDungeonCreated(Dungeon dungeon)
        {
            CurrentDungeon = dungeon;
            Initialize();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Gate"))
            {
            }
        }
    }
}