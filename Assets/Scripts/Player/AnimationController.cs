using System;
using UnityEngine;

namespace Player
{
    public class AnimationController : MonoBehaviour
    {
        public Animator animator;
        
        private static readonly int IsRunning = Animator.StringToHash("IsRunning");
        private static readonly int Attack = Animator.StringToHash("Attack");


        private void Awake()
        {
            WeaponController.OnWeaponFired += WeaponController_OnWeaponFired;
            MovementController.OnMovementStateChanged += MovementController_OnMovementStateChanged;
        }

        private void MovementController_OnMovementStateChanged(bool state)
        {
            ToggleRun(state);
        }

        private void WeaponController_OnWeaponFired()
        {
            // AttackAnim();
        }

        public void ToggleRun(bool toggle)
        {
            animator.SetBool(IsRunning, toggle);
        }

        public void AttackAnim()
        {
            animator.SetTrigger(Attack);
        }
    }
}