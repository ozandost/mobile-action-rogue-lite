using System;
using Enemies;
using Sirenix.OdinInspector;
using UnityEngine;
using Weapons;

namespace Player
{
    public class WeaponController : MonoBehaviour
    {
        public BWeapon CurrentWeapon { get; private set; }
        private Enemy CurrentEnemy { get; set; }

        public static event Action OnWeaponFired;

        [Button]
        public void Initialize()
        {
            var weapon = GetComponentInChildren<BWeapon>();
            ChangeWeapon(weapon);

            MovementController.OnMovementStateChanged += this.OnMovementStateChanged;
            CombatController.OnTargetChanged += this.OnTargetChanged;
        }

        public void OnMovementStateChanged(bool isMoving)
        {
            if (CurrentEnemy == null) return;
            if (CurrentWeapon) CurrentWeapon.ToggleAttack(!isMoving);
        }


        public void ChangeWeapon(BWeapon weapon)
        {
            if (CurrentWeapon)
            {
                CurrentWeapon.OnFired -= CurrentWeapon_OnFired;
                CurrentWeapon.Finalize();
            }

            CurrentWeapon = weapon;
            CurrentWeapon.Initialize();
            CurrentWeapon.OnFired += CurrentWeapon_OnFired;
        }

        private void CurrentWeapon_OnFired()
        {
            OnWeaponFired?.Invoke();
        }

        public void OnTargetChanged(Enemy obj)
        {
            CurrentEnemy = obj;
            CurrentWeapon.ChangeTarget(CurrentEnemy);
        }
    }
}