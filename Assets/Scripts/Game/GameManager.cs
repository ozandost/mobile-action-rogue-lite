using System;
using DungeonSystem;
using Player;
using UnityEngine;

namespace Game
{
    public class GameManager : MonoBehaviour
    {
        [Header("References")] public PlayerController player;
        public DungeonManager dungeonManager;
        
        private void Awake()
        {
            dungeonManager.CreateDungeon();
        }
    }
}